package com.assamadda.models;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class MasterOrganisation extends Auditable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long organisationId;

	@NotBlank(message = "*Please enter a name")
    private String name;

    private String description;

    @Lob
    @NotNull(message = "* Image is required")
    private byte[] image;

    @Column(columnDefinition = "tinyint(1) default 0")
    private Boolean isActive = true;



    public MasterOrganisation() {
    }

    public MasterOrganisation(Long organisationId, String name, String description, byte[] image, Boolean isActive) {
        this.organisationId = organisationId;
        this.name = name;
        this.description = description;
        this.image = image;
        this.isActive = isActive;
    }

    public Long getOrganisationId() {
        return this.organisationId;
    }

    public void setOrganisationId(Long organisationId) {
        this.organisationId = organisationId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getImage() {
        return this.image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public Boolean isIsActive() {
        return this.isActive;
    }

    public Boolean getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public MasterOrganisation organisationId(Long organisationId) {
        this.organisationId = organisationId;
        return this;
    }

    public MasterOrganisation name(String name) {
        this.name = name;
        return this;
    }

    public MasterOrganisation description(String description) {
        this.description = description;
        return this;
    }

    public MasterOrganisation image(byte[] image) {
        this.image = image;
        return this;
    }

    public MasterOrganisation isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof MasterOrganisation)) {
            return false;
        }
        MasterOrganisation masterOrganisation = (MasterOrganisation) o;
        return Objects.equals(organisationId, masterOrganisation.organisationId) && Objects.equals(name, masterOrganisation.name) && Objects.equals(description, masterOrganisation.description) && Objects.equals(image, masterOrganisation.image) && Objects.equals(isActive, masterOrganisation.isActive);
    }

    @Override
    public int hashCode() {
        return Objects.hash(organisationId, name, description, image, isActive);
    }

    @Override
    public String toString() {
        return "{" +
            " organisationId='" + getOrganisationId() + "'" +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", image='" + getImage() + "'" +
            ", isActive='" + isIsActive() + "'" +
            "}";
    }


}