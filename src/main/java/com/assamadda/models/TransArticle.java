package com.assamadda.models;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
public class TransArticle extends Auditable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long articleId;
      
    @Size(max = 1000)
	@NotBlank(message = "* Please enter a title")
    private String title;
      
    @Size(max = 5000)
	@NotBlank(message = "* Please enter a description")
    private String description;

    @Column(name = "content", columnDefinition = "longtext")
    private String content;
    
    @Column(name = "keywords")
    private String keywords;

    @Column(columnDefinition = "tinyint(1) default 0")
    private Boolean isActive = true;

    @Size(max = 1050)
    private String slug;
    
    private String visibleTo;


    public TransArticle() {
    }

    public TransArticle(Long articleId, String title, String description, String content, String keywords, Boolean isActive, String slug, String visibleTo) {
        this.articleId = articleId;
        this.title = title;
        this.description = description;
        this.content = content;
        this.keywords = keywords;
        this.isActive = isActive;
        this.slug = slug;
        this.visibleTo = visibleTo;
    }

    public Long getArticleId() {
        return this.articleId;
    }

    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getKeywords() {
        return this.keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public Boolean isIsActive() {
        return this.isActive;
    }

    public Boolean getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getSlug() {
        return this.slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getVisibleTo() {
        return this.visibleTo;
    }

    public void setVisibleTo(String visibleTo) {
        this.visibleTo = visibleTo;
    }

    public TransArticle articleId(Long articleId) {
        this.articleId = articleId;
        return this;
    }

    public TransArticle title(String title) {
        this.title = title;
        return this;
    }

    public TransArticle description(String description) {
        this.description = description;
        return this;
    }

    public TransArticle content(String content) {
        this.content = content;
        return this;
    }

    public TransArticle keywords(String keywords) {
        this.keywords = keywords;
        return this;
    }

    public TransArticle isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public TransArticle slug(String slug) {
        this.slug = slug;
        return this;
    }

    public TransArticle visibleTo(String visibleTo) {
        this.visibleTo = visibleTo;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof TransArticle)) {
            return false;
        }
        TransArticle transArticle = (TransArticle) o;
        return Objects.equals(articleId, transArticle.articleId) && Objects.equals(title, transArticle.title) && Objects.equals(description, transArticle.description) && Objects.equals(content, transArticle.content) && Objects.equals(keywords, transArticle.keywords) && Objects.equals(isActive, transArticle.isActive) && Objects.equals(slug, transArticle.slug) && Objects.equals(visibleTo, transArticle.visibleTo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(articleId, title, description, content, keywords, isActive, slug, visibleTo);
    }

    @Override
    public String toString() {
        return "{" +
            " articleId='" + getArticleId() + "'" +
            ", title='" + getTitle() + "'" +
            ", description='" + getDescription() + "'" +
            ", content='" + getContent() + "'" +
            ", keywords='" + getKeywords() + "'" +
            ", isActive='" + isIsActive() + "'" +
            ", slug='" + getSlug() + "'" +
            ", visibleTo='" + getVisibleTo() + "'" +
            "}";
    }

}