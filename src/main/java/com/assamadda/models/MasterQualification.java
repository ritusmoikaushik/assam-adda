package com.assamadda.models;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Entity
public class MasterQualification extends Auditable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long qualificationId;

	@NotBlank(message = "*Please enter a name")
    private String name;

    private String description;

    private String slug;

    @Column(columnDefinition = "tinyint(1) default 0")
    private Boolean isActive = false;



    public MasterQualification() {
    }

    public MasterQualification(Long qualificationId, String name, String description, String slug, Boolean isActive) {
        this.qualificationId = qualificationId;
        this.name = name;
        this.description = description;
        this.slug = slug;
        this.isActive = isActive;
    }

    public Long getQualificationId() {
        return this.qualificationId;
    }

    public void setQualificationId(Long qualificationId) {
        this.qualificationId = qualificationId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSlug() {
        return this.slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Boolean isIsActive() {
        return this.isActive;
    }

    public Boolean getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public MasterQualification qualificationId(Long qualificationId) {
        this.qualificationId = qualificationId;
        return this;
    }

    public MasterQualification name(String name) {
        this.name = name;
        return this;
    }

    public MasterQualification description(String description) {
        this.description = description;
        return this;
    }

    public MasterQualification slug(String slug) {
        this.slug = slug;
        return this;
    }

    public MasterQualification isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof MasterQualification)) {
            return false;
        }
        MasterQualification masterQualification = (MasterQualification) o;
        return Objects.equals(qualificationId, masterQualification.qualificationId) && Objects.equals(name, masterQualification.name) && Objects.equals(description, masterQualification.description) && Objects.equals(slug, masterQualification.slug) && Objects.equals(isActive, masterQualification.isActive);
    }

    @Override
    public int hashCode() {
        return Objects.hash(qualificationId, name, description, slug, isActive);
    }

    @Override
    public String toString() {
        return "{" +
            " qualificationId='" + getQualificationId() + "'" +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", slug='" + getSlug() + "'" +
            ", isActive='" + isIsActive() + "'" +
            "}";
    }
    
}