package com.assamadda.models;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Entity
public class MasterDistrict extends Auditable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long districtId;

	@NotBlank(message = "*Please enter a name")
    private String name;

    private String description;

    @Column(columnDefinition = "tinyint(1) default 0")
    private Boolean isActive = true;


    public MasterDistrict() {
    }

    public MasterDistrict(Long districtId, String name, String description, Boolean isActive) {
        this.districtId = districtId;
        this.name = name;
        this.description = description;
        this.isActive = isActive;
    }

    public Long getDistrictId() {
        return this.districtId;
    }

    public void setDistrictId(Long districtId) {
        this.districtId = districtId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean isIsActive() {
        return this.isActive;
    }

    public Boolean getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public MasterDistrict districtId(Long districtId) {
        this.districtId = districtId;
        return this;
    }

    public MasterDistrict name(String name) {
        this.name = name;
        return this;
    }

    public MasterDistrict description(String description) {
        this.description = description;
        return this;
    }

    public MasterDistrict isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof MasterDistrict)) {
            return false;
        }
        MasterDistrict masterDistrict = (MasterDistrict) o;
        return Objects.equals(districtId, masterDistrict.districtId) && Objects.equals(name, masterDistrict.name) && Objects.equals(description, masterDistrict.description) && Objects.equals(isActive, masterDistrict.isActive);
    }

    @Override
    public int hashCode() {
        return Objects.hash(districtId, name, description, isActive);
    }

    @Override
    public String toString() {
        return "{" +
            " districtId='" + getDistrictId() + "'" +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", isActive='" + isIsActive() + "'" +
            "}";
    }

}