package com.assamadda.models;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
public class TransSubscribe  extends Auditable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long subscriberId;
      
    @Size(max = 200)
	@NotBlank(message = "* Email is required")
    private String email ;

    @Column(columnDefinition = "tinyint(1) default 0")
    private Boolean isActive = false;


    public TransSubscribe() {
    }

    public TransSubscribe(Long subscriberId, String email, Boolean isActive) {
        this.subscriberId = subscriberId;
        this.email = email;
        this.isActive = isActive;
    }

    public Long getSubscriberId() {
        return this.subscriberId;
    }

    public void setSubscriberId(Long subscriberId) {
        this.subscriberId = subscriberId;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean isIsActive() {
        return this.isActive;
    }

    public Boolean getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public TransSubscribe subscriberId(Long subscriberId) {
        this.subscriberId = subscriberId;
        return this;
    }

    public TransSubscribe email(String email) {
        this.email = email;
        return this;
    }

    public TransSubscribe isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof TransSubscribe)) {
            return false;
        }
        TransSubscribe transSubscribe = (TransSubscribe) o;
        return Objects.equals(subscriberId, transSubscribe.subscriberId) && Objects.equals(email, transSubscribe.email) && Objects.equals(isActive, transSubscribe.isActive);
    }

    @Override
    public int hashCode() {
        return Objects.hash(subscriberId, email, isActive);
    }

    @Override
    public String toString() {
        return "{" +
            " subscriberId='" + getSubscriberId() + "'" +
            ", email='" + getEmail() + "'" +
            ", isActive='" + isIsActive() + "'" +
            "}";
    }

}