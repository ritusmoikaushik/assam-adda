package com.assamadda.models;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class TransCurrentAffair extends Auditable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long caId;

    @Column(name = "content", columnDefinition = "longtext")
	@NotBlank(message = "* Please enter content")
    private String content;

    @Column(columnDefinition = "tinyint(1) default 0")
    private Boolean isActive = true;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd") 
    private Date postDate;

    private String visibleTo;


    public TransCurrentAffair() {
    }

    public TransCurrentAffair(Long caId, String content, Boolean isActive, Date postDate, String visibleTo) {
        this.caId = caId;
        this.content = content;
        this.isActive = isActive;
        this.postDate = postDate;
        this.visibleTo = visibleTo;
    }

    public Long getCaId() {
        return this.caId;
    }

    public void setCaId(Long caId) {
        this.caId = caId;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean isIsActive() {
        return this.isActive;
    }

    public Boolean getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Date getPostDate() {
        return this.postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public String getVisibleTo() {
        return this.visibleTo;
    }

    public void setVisibleTo(String visibleTo) {
        this.visibleTo = visibleTo;
    }

    public TransCurrentAffair caId(Long caId) {
        this.caId = caId;
        return this;
    }

    public TransCurrentAffair content(String content) {
        this.content = content;
        return this;
    }

    public TransCurrentAffair isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public TransCurrentAffair postDate(Date postDate) {
        this.postDate = postDate;
        return this;
    }

    public TransCurrentAffair visibleTo(String visibleTo) {
        this.visibleTo = visibleTo;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof TransCurrentAffair)) {
            return false;
        }
        TransCurrentAffair transCurrentAffair = (TransCurrentAffair) o;
        return Objects.equals(caId, transCurrentAffair.caId) && Objects.equals(content, transCurrentAffair.content) && Objects.equals(isActive, transCurrentAffair.isActive) && Objects.equals(postDate, transCurrentAffair.postDate) && Objects.equals(visibleTo, transCurrentAffair.visibleTo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(caId, content, isActive, postDate, visibleTo);
    }

    @Override
    public String toString() {
        return "{" +
            " caId='" + getCaId() + "'" +
            ", content='" + getContent() + "'" +
            ", isActive='" + isIsActive() + "'" +
            ", postDate='" + getPostDate() + "'" +
            ", visibleTo='" + getVisibleTo() + "'" +
            "}";
    }

}