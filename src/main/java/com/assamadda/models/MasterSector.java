package com.assamadda.models;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Entity
public class MasterSector extends Auditable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long sectorId;

	@NotBlank(message = "*Please enter a name")
    private String name;

    private String description;

    @Column(columnDefinition = "tinyint(1) default 0")
    private Boolean isActive = true;


    public MasterSector() {
    }

    public MasterSector(Long sectorId, String name, String description, Boolean isActive) {
        this.sectorId = sectorId;
        this.name = name;
        this.description = description;
        this.isActive = isActive;
    }

    public Long getSectorId() {
        return this.sectorId;
    }

    public void setSectorId(Long sectorId) {
        this.sectorId = sectorId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean isIsActive() {
        return this.isActive;
    }

    public Boolean getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public MasterSector sectorId(Long sectorId) {
        this.sectorId = sectorId;
        return this;
    }

    public MasterSector name(String name) {
        this.name = name;
        return this;
    }

    public MasterSector description(String description) {
        this.description = description;
        return this;
    }

    public MasterSector isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof MasterSector)) {
            return false;
        }
        MasterSector masterSector = (MasterSector) o;
        return Objects.equals(sectorId, masterSector.sectorId) && Objects.equals(name, masterSector.name) && Objects.equals(description, masterSector.description) && Objects.equals(isActive, masterSector.isActive);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sectorId, name, description, isActive);
    }

    @Override
    public String toString() {
        return "{" +
            " sectorId='" + getSectorId() + "'" +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", isActive='" + isIsActive() + "'" +
            "}";
    }

}