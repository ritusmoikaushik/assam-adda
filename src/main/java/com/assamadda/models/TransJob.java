package com.assamadda.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class TransJob extends Auditable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long jobId;
      
    @Size(max = 1000)
	@NotBlank(message = "* Please enter a title")
    private String title;
      
    @Size(max = 5000)
	@NotBlank(message = "* Please enter a description")
    private String description;

    @Column(name = "content", columnDefinition = "longtext")
    private String content;

    @NotBlank(message = "* Please select a category")
    private String category;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "join_job_sector", joinColumns = @JoinColumn(name = "job_id"), inverseJoinColumns = @JoinColumn(name = "sector_id"))
    @Column(nullable = true)   
    private List<MasterSector> sectors = new ArrayList<>();

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "join_job_district", joinColumns = @JoinColumn(name = "job_id"), inverseJoinColumns = @JoinColumn(name = "district_id"))
    @Column(nullable = true)   
    private List<MasterDistrict> districts = new ArrayList<>();

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "join_job_qualification", joinColumns = @JoinColumn(name = "job_id"), inverseJoinColumns = @JoinColumn(name = "qualification_id"))
    @Column(nullable = true)   
    private List<MasterQualification> qualifications = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="fk_organisation_id", nullable = true) 
    private MasterOrganisation organisation;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd") 
    private Date startDate;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd") 
    private Date endDate;

    @Column(name = "keywords")
    private String keywords;

    @Column(columnDefinition = "tinyint(1) default 0")
    private Boolean isActive = true;

    @Size(max = 1050)
    private String slug;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd") 
    private Date postDate;

    private String visibleTo;


    public TransJob() {
    }

    public TransJob(Long jobId, String title, String description, String content, String category, List<MasterSector> sectors, List<MasterDistrict> districts, List<MasterQualification> qualifications, MasterOrganisation organisation, Date startDate, Date endDate, String keywords, Boolean isActive, String slug, Date postDate, String visibleTo) {
        this.jobId = jobId;
        this.title = title;
        this.description = description;
        this.content = content;
        this.category = category;
        this.sectors = sectors;
        this.districts = districts;
        this.qualifications = qualifications;
        this.organisation = organisation;
        this.startDate = startDate;
        this.endDate = endDate;
        this.keywords = keywords;
        this.isActive = isActive;
        this.slug = slug;
        this.postDate = postDate;
        this.visibleTo = visibleTo;
    }

    public Long getJobId() {
        return this.jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<MasterSector> getSectors() {
        return this.sectors;
    }

    public void setSectors(List<MasterSector> sectors) {
        this.sectors = sectors;
    }

    public List<MasterDistrict> getDistricts() {
        return this.districts;
    }

    public void setDistricts(List<MasterDistrict> districts) {
        this.districts = districts;
    }

    public List<MasterQualification> getQualifications() {
        return this.qualifications;
    }

    public void setQualifications(List<MasterQualification> qualifications) {
        this.qualifications = qualifications;
    }

    public MasterOrganisation getOrganisation() {
        return this.organisation;
    }

    public void setOrganisation(MasterOrganisation organisation) {
        this.organisation = organisation;
    }

    public Date getStartDate() {
        return this.startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return this.endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getKeywords() {
        return this.keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public Boolean isIsActive() {
        return this.isActive;
    }

    public Boolean getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getSlug() {
        return this.slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Date getPostDate() {
        return this.postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public String getVisibleTo() {
        return this.visibleTo;
    }

    public void setVisibleTo(String visibleTo) {
        this.visibleTo = visibleTo;
    }

    public TransJob jobId(Long jobId) {
        this.jobId = jobId;
        return this;
    }

    public TransJob title(String title) {
        this.title = title;
        return this;
    }

    public TransJob description(String description) {
        this.description = description;
        return this;
    }

    public TransJob content(String content) {
        this.content = content;
        return this;
    }

    public TransJob category(String category) {
        this.category = category;
        return this;
    }

    public TransJob sectors(List<MasterSector> sectors) {
        this.sectors = sectors;
        return this;
    }

    public TransJob districts(List<MasterDistrict> districts) {
        this.districts = districts;
        return this;
    }

    public TransJob qualifications(List<MasterQualification> qualifications) {
        this.qualifications = qualifications;
        return this;
    }

    public TransJob organisation(MasterOrganisation organisation) {
        this.organisation = organisation;
        return this;
    }

    public TransJob startDate(Date startDate) {
        this.startDate = startDate;
        return this;
    }

    public TransJob endDate(Date endDate) {
        this.endDate = endDate;
        return this;
    }

    public TransJob keywords(String keywords) {
        this.keywords = keywords;
        return this;
    }

    public TransJob isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public TransJob slug(String slug) {
        this.slug = slug;
        return this;
    }

    public TransJob postDate(Date postDate) {
        this.postDate = postDate;
        return this;
    }

    public TransJob visibleTo(String visibleTo) {
        this.visibleTo = visibleTo;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof TransJob)) {
            return false;
        }
        TransJob transJob = (TransJob) o;
        return Objects.equals(jobId, transJob.jobId) && Objects.equals(title, transJob.title) && Objects.equals(description, transJob.description) && Objects.equals(content, transJob.content) && Objects.equals(category, transJob.category) && Objects.equals(sectors, transJob.sectors) && Objects.equals(districts, transJob.districts) && Objects.equals(qualifications, transJob.qualifications) && Objects.equals(organisation, transJob.organisation) && Objects.equals(startDate, transJob.startDate) && Objects.equals(endDate, transJob.endDate) && Objects.equals(keywords, transJob.keywords) && Objects.equals(isActive, transJob.isActive) && Objects.equals(slug, transJob.slug) && Objects.equals(postDate, transJob.postDate) && Objects.equals(visibleTo, transJob.visibleTo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(jobId, title, description, content, category, sectors, districts, qualifications, organisation, startDate, endDate, keywords, isActive, slug, postDate, visibleTo);
    }

    @Override
    public String toString() {
        return "{" +
            " jobId='" + getJobId() + "'" +
            ", title='" + getTitle() + "'" +
            ", description='" + getDescription() + "'" +
            ", content='" + getContent() + "'" +
            ", category='" + getCategory() + "'" +
            ", sectors='" + getSectors() + "'" +
            ", districts='" + getDistricts() + "'" +
            ", qualifications='" + getQualifications() + "'" +
            ", organisation='" + getOrganisation() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", keywords='" + getKeywords() + "'" +
            ", isActive='" + isIsActive() + "'" +
            ", slug='" + getSlug() + "'" +
            ", postDate='" + getPostDate() + "'" +
            ", visibleTo='" + getVisibleTo() + "'" +
            "}";
    }

}