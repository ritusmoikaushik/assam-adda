package com.assamadda.payloads;

import java.util.Objects;

import com.assamadda.models.Auditable;

public class ArticlePayload extends Auditable{
    private Long articleId;
    private String title;
    private String description;
    private String content;
    private String keywords;
    private Boolean isActive = true;
    private String visibleTo;
    private String slug;



    public ArticlePayload() {
    }

    public ArticlePayload(Long articleId, String title, String description, String content, String keywords, Boolean isActive, String visibleTo, String slug) {
        this.articleId = articleId;
        this.title = title;
        this.description = description;
        this.content = content;
        this.keywords = keywords;
        this.isActive = isActive;
        this.visibleTo = visibleTo;
        this.slug = slug;
    }

    public Long getArticleId() {
        return this.articleId;
    }

    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getKeywords() {
        return this.keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public Boolean isIsActive() {
        return this.isActive;
    }

    public Boolean getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getVisibleTo() {
        return this.visibleTo;
    }

    public void setVisibleTo(String visibleTo) {
        this.visibleTo = visibleTo;
    }

    public String getSlug() {
        return this.slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public ArticlePayload articleId(Long articleId) {
        this.articleId = articleId;
        return this;
    }

    public ArticlePayload title(String title) {
        this.title = title;
        return this;
    }

    public ArticlePayload description(String description) {
        this.description = description;
        return this;
    }

    public ArticlePayload content(String content) {
        this.content = content;
        return this;
    }

    public ArticlePayload keywords(String keywords) {
        this.keywords = keywords;
        return this;
    }

    public ArticlePayload isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public ArticlePayload visibleTo(String visibleTo) {
        this.visibleTo = visibleTo;
        return this;
    }

    public ArticlePayload slug(String slug) {
        this.slug = slug;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof ArticlePayload)) {
            return false;
        }
        ArticlePayload articlePayload = (ArticlePayload) o;
        return Objects.equals(articleId, articlePayload.articleId) && Objects.equals(title, articlePayload.title) && Objects.equals(description, articlePayload.description) && Objects.equals(content, articlePayload.content) && Objects.equals(keywords, articlePayload.keywords) && Objects.equals(isActive, articlePayload.isActive) && Objects.equals(visibleTo, articlePayload.visibleTo) && Objects.equals(slug, articlePayload.slug);
    }

    @Override
    public int hashCode() {
        return Objects.hash(articleId, title, description, content, keywords, isActive, visibleTo, slug);
    }

    @Override
    public String toString() {
        return "{" +
            " articleId='" + getArticleId() + "'" +
            ", title='" + getTitle() + "'" +
            ", description='" + getDescription() + "'" +
            ", content='" + getContent() + "'" +
            ", keywords='" + getKeywords() + "'" +
            ", isActive='" + isIsActive() + "'" +
            ", visibleTo='" + getVisibleTo() + "'" +
            ", slug='" + getSlug() + "'" +
            "}";
    }
    
}