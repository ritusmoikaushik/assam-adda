package com.assamadda.payloads;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * PageableObjectPayload
 */
public class PageableObjectPayload {

    PageDetails pageDetails;
    List<?> objectList = new ArrayList<>();
    List<?> objectList2 = new ArrayList<>();
    List<?> objectList3 = new ArrayList<>();
    List<?> objectList4 = new ArrayList<>();
    List<BreadCrumbPayload> breadcrumbList = new ArrayList<>();
    Object currentObject;
    Object currentObject2;
    Object currentObject3;
    Object currentObject4;


    public PageableObjectPayload() {
    }

    public PageableObjectPayload(PageDetails pageDetails, List<?> objectList, List<?> objectList2, List<?> objectList3, List<?> objectList4, List<BreadCrumbPayload> breadcrumbList, Object currentObject, Object currentObject2, Object currentObject3, Object currentObject4) {
        this.pageDetails = pageDetails;
        this.objectList = objectList;
        this.objectList2 = objectList2;
        this.objectList3 = objectList3;
        this.objectList4 = objectList4;
        this.breadcrumbList = breadcrumbList;
        this.currentObject = currentObject;
        this.currentObject2 = currentObject2;
        this.currentObject3 = currentObject3;
        this.currentObject4 = currentObject4;
    }

    public PageDetails getPageDetails() {
        return this.pageDetails;
    }

    public void setPageDetails(PageDetails pageDetails) {
        this.pageDetails = pageDetails;
    }

    public List<?> getObjectList() {
        return this.objectList;
    }

    public void setObjectList(List<?> objectList) {
        this.objectList = objectList;
    }

    public List<?> getObjectList2() {
        return this.objectList2;
    }

    public void setObjectList2(List<?> objectList2) {
        this.objectList2 = objectList2;
    }

    public List<?> getObjectList3() {
        return this.objectList3;
    }

    public void setObjectList3(List<?> objectList3) {
        this.objectList3 = objectList3;
    }

    public List<?> getObjectList4() {
        return this.objectList4;
    }

    public void setObjectList4(List<?> objectList4) {
        this.objectList4 = objectList4;
    }

    public List<BreadCrumbPayload> getBreadcrumbList() {
        return this.breadcrumbList;
    }

    public void setBreadcrumbList(List<BreadCrumbPayload> breadcrumbList) {
        this.breadcrumbList = breadcrumbList;
    }

    public Object getCurrentObject() {
        return this.currentObject;
    }

    public void setCurrentObject(Object currentObject) {
        this.currentObject = currentObject;
    }

    public Object getCurrentObject2() {
        return this.currentObject2;
    }

    public void setCurrentObject2(Object currentObject2) {
        this.currentObject2 = currentObject2;
    }

    public Object getCurrentObject3() {
        return this.currentObject3;
    }

    public void setCurrentObject3(Object currentObject3) {
        this.currentObject3 = currentObject3;
    }

    public Object getCurrentObject4() {
        return this.currentObject4;
    }

    public void setCurrentObject4(Object currentObject4) {
        this.currentObject4 = currentObject4;
    }

    public PageableObjectPayload pageDetails(PageDetails pageDetails) {
        this.pageDetails = pageDetails;
        return this;
    }

    public PageableObjectPayload objectList(List<?> objectList) {
        this.objectList = objectList;
        return this;
    }

    public PageableObjectPayload objectList2(List<?> objectList2) {
        this.objectList2 = objectList2;
        return this;
    }

    public PageableObjectPayload objectList3(List<?> objectList3) {
        this.objectList3 = objectList3;
        return this;
    }

    public PageableObjectPayload objectList4(List<?> objectList4) {
        this.objectList4 = objectList4;
        return this;
    }

    public PageableObjectPayload breadcrumbList(List<BreadCrumbPayload> breadcrumbList) {
        this.breadcrumbList = breadcrumbList;
        return this;
    }

    public PageableObjectPayload currentObject(Object currentObject) {
        this.currentObject = currentObject;
        return this;
    }

    public PageableObjectPayload currentObject2(Object currentObject2) {
        this.currentObject2 = currentObject2;
        return this;
    }

    public PageableObjectPayload currentObject3(Object currentObject3) {
        this.currentObject3 = currentObject3;
        return this;
    }

    public PageableObjectPayload currentObject4(Object currentObject4) {
        this.currentObject4 = currentObject4;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof PageableObjectPayload)) {
            return false;
        }
        PageableObjectPayload pageableObjectPayload = (PageableObjectPayload) o;
        return Objects.equals(pageDetails, pageableObjectPayload.pageDetails) && Objects.equals(objectList, pageableObjectPayload.objectList) && Objects.equals(objectList2, pageableObjectPayload.objectList2) && Objects.equals(objectList3, pageableObjectPayload.objectList3) && Objects.equals(objectList4, pageableObjectPayload.objectList4) && Objects.equals(breadcrumbList, pageableObjectPayload.breadcrumbList) && Objects.equals(currentObject, pageableObjectPayload.currentObject) && Objects.equals(currentObject2, pageableObjectPayload.currentObject2) && Objects.equals(currentObject3, pageableObjectPayload.currentObject3) && Objects.equals(currentObject4, pageableObjectPayload.currentObject4);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pageDetails, objectList, objectList2, objectList3, objectList4, breadcrumbList, currentObject, currentObject2, currentObject3, currentObject4);
    }

    @Override
    public String toString() {
        return "{" +
            " pageDetails='" + getPageDetails() + "'" +
            ", objectList='" + getObjectList() + "'" +
            ", objectList2='" + getObjectList2() + "'" +
            ", objectList3='" + getObjectList3() + "'" +
            ", objectList4='" + getObjectList4() + "'" +
            ", breadcrumbList='" + getBreadcrumbList() + "'" +
            ", currentObject='" + getCurrentObject() + "'" +
            ", currentObject2='" + getCurrentObject2() + "'" +
            ", currentObject3='" + getCurrentObject3() + "'" +
            ", currentObject4='" + getCurrentObject4() + "'" +
            "}";
    }


}