package com.assamadda.payloads;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import com.assamadda.models.Auditable;
import com.assamadda.models.MasterDistrict;
import com.assamadda.models.MasterOrganisation;
import com.assamadda.models.MasterQualification;
import com.assamadda.models.MasterSector;

public class JobPayload extends Auditable{
    private Long jobId;
    private String title;
    private String description;
    private String content;
    private String category;
    private List<MasterSector> sectors = new ArrayList<>();
    private List<MasterDistrict> districts = new ArrayList<>();
    private List<MasterQualification> qualifications = new ArrayList<>();
    private MasterOrganisation organisation;
    private Date startDate;
    private Date endDate;
    private String keywords;
    private Boolean isActive = true;
    private Date postDate;
    private String visibleTo;
    private String slug;


    public JobPayload() {
    }

    public JobPayload(Long jobId, String title, String description, String content, String category, List<MasterSector> sectors, List<MasterDistrict> districts, List<MasterQualification> qualifications, MasterOrganisation organisation, Date startDate, Date endDate, String keywords, Boolean isActive, Date postDate, String visibleTo, String slug) {
        this.jobId = jobId;
        this.title = title;
        this.description = description;
        this.content = content;
        this.category = category;
        this.sectors = sectors;
        this.districts = districts;
        this.qualifications = qualifications;
        this.organisation = organisation;
        this.startDate = startDate;
        this.endDate = endDate;
        this.keywords = keywords;
        this.isActive = isActive;
        this.postDate = postDate;
        this.visibleTo = visibleTo;
        this.slug = slug;
    }

    public Long getJobId() {
        return this.jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<MasterSector> getSectors() {
        return this.sectors;
    }

    public void setSectors(List<MasterSector> sectors) {
        this.sectors = sectors;
    }

    public List<MasterDistrict> getDistricts() {
        return this.districts;
    }

    public void setDistricts(List<MasterDistrict> districts) {
        this.districts = districts;
    }

    public List<MasterQualification> getQualifications() {
        return this.qualifications;
    }

    public void setQualifications(List<MasterQualification> qualifications) {
        this.qualifications = qualifications;
    }

    public MasterOrganisation getOrganisation() {
        return this.organisation;
    }

    public void setOrganisation(MasterOrganisation organisation) {
        this.organisation = organisation;
    }

    public Date getStartDate() {
        return this.startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return this.endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getKeywords() {
        return this.keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public Boolean isIsActive() {
        return this.isActive;
    }

    public Boolean getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Date getPostDate() {
        return this.postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public String getVisibleTo() {
        return this.visibleTo;
    }

    public void setVisibleTo(String visibleTo) {
        this.visibleTo = visibleTo;
    }

    public String getSlug() {
        return this.slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public JobPayload jobId(Long jobId) {
        this.jobId = jobId;
        return this;
    }

    public JobPayload title(String title) {
        this.title = title;
        return this;
    }

    public JobPayload description(String description) {
        this.description = description;
        return this;
    }

    public JobPayload content(String content) {
        this.content = content;
        return this;
    }

    public JobPayload category(String category) {
        this.category = category;
        return this;
    }

    public JobPayload sectors(List<MasterSector> sectors) {
        this.sectors = sectors;
        return this;
    }

    public JobPayload districts(List<MasterDistrict> districts) {
        this.districts = districts;
        return this;
    }

    public JobPayload qualifications(List<MasterQualification> qualifications) {
        this.qualifications = qualifications;
        return this;
    }

    public JobPayload organisation(MasterOrganisation organisation) {
        this.organisation = organisation;
        return this;
    }

    public JobPayload startDate(Date startDate) {
        this.startDate = startDate;
        return this;
    }

    public JobPayload endDate(Date endDate) {
        this.endDate = endDate;
        return this;
    }

    public JobPayload keywords(String keywords) {
        this.keywords = keywords;
        return this;
    }

    public JobPayload isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public JobPayload postDate(Date postDate) {
        this.postDate = postDate;
        return this;
    }

    public JobPayload visibleTo(String visibleTo) {
        this.visibleTo = visibleTo;
        return this;
    }

    public JobPayload slug(String slug) {
        this.slug = slug;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof JobPayload)) {
            return false;
        }
        JobPayload jobPayload = (JobPayload) o;
        return Objects.equals(jobId, jobPayload.jobId) && Objects.equals(title, jobPayload.title) && Objects.equals(description, jobPayload.description) && Objects.equals(content, jobPayload.content) && Objects.equals(category, jobPayload.category) && Objects.equals(sectors, jobPayload.sectors) && Objects.equals(districts, jobPayload.districts) && Objects.equals(qualifications, jobPayload.qualifications) && Objects.equals(organisation, jobPayload.organisation) && Objects.equals(startDate, jobPayload.startDate) && Objects.equals(endDate, jobPayload.endDate) && Objects.equals(keywords, jobPayload.keywords) && Objects.equals(isActive, jobPayload.isActive) && Objects.equals(postDate, jobPayload.postDate) && Objects.equals(visibleTo, jobPayload.visibleTo) && Objects.equals(slug, jobPayload.slug);
    }

    @Override
    public int hashCode() {
        return Objects.hash(jobId, title, description, content, category, sectors, districts, qualifications, organisation, startDate, endDate, keywords, isActive, postDate, visibleTo, slug);
    }

    @Override
    public String toString() {
        return "{" +
            " jobId='" + getJobId() + "'" +
            ", title='" + getTitle() + "'" +
            ", description='" + getDescription() + "'" +
            ", content='" + getContent() + "'" +
            ", category='" + getCategory() + "'" +
            ", sectors='" + getSectors() + "'" +
            ", districts='" + getDistricts() + "'" +
            ", qualifications='" + getQualifications() + "'" +
            ", organisation='" + getOrganisation() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", keywords='" + getKeywords() + "'" +
            ", isActive='" + isIsActive() + "'" +
            ", postDate='" + getPostDate() + "'" +
            ", visibleTo='" + getVisibleTo() + "'" +
            ", slug='" + getSlug() + "'" +
            "}";
    }

    
}