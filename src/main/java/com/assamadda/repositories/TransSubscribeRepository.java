package com.assamadda.repositories;

import com.assamadda.models.TransSubscribe;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * TransSubscribeRepository
 */
@Repository
public interface TransSubscribeRepository extends JpaRepository<TransSubscribe, Long>{

	TransSubscribe findByEmail(String email);
    
}