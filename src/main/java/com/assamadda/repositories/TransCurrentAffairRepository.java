package com.assamadda.repositories;

import java.util.Date;
import java.util.List;

import com.assamadda.models.TransCurrentAffair;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransCurrentAffairRepository extends JpaRepository<TransCurrentAffair, Long>{

	List<TransCurrentAffair> findByPostDateBetweenAndIsActiveOrderByPostDateDesc(Date startDate, Date endDate,
			boolean b);

	Page<TransCurrentAffair> findByIsActiveAndVisibleTo(boolean b, String string, Pageable sortedByPostDesc);
    
}