package com.assamadda.repositories;

import java.util.Date;
import java.util.List;

import com.assamadda.models.TransJob;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransJobRepository extends JpaRepository<TransJob, Long>{

	List<TransJob> findByCreatedAtBetweenAndIsActiveOrderByCreatedAtDesc(Date startDate, Date endDate, boolean b);

	TransJob findBySlugAndIsActive(String jobSlug, boolean b);

	Page<TransJob> findByIsActiveAndVisibleTo(boolean b, String visibleTo, Pageable sortedByCapturedAtDesc);

	TransJob findBySlugAndVisibleToAndIsActive(String slug, String string, boolean b);

	Page<TransJob> findByTitleContainingAndIsActiveAndVisibleTo(String searchText, boolean b, String string,
			Pageable sortedByPostDesc);

	Page<TransJob> findByDistricts_descriptionAndIsActiveAndVisibleTo(String district, boolean b, String string,
			Pageable sortedByPostDesc);

	Page<TransJob> findBySectors_descriptionAndIsActiveAndVisibleTo(String sector, boolean b, String string,
			Pageable sortedByPostDesc);

	Page<TransJob> findByCategoryAndIsActiveAndVisibleTo(String category, boolean b, String string,
			Pageable sortedByPostDesc);

	Page<TransJob> findByQualifications_slugAndIsActiveAndVisibleTo(String qualification, boolean b, String string,
			Pageable sortedByPostDesc);

}