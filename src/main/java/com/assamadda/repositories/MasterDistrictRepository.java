package com.assamadda.repositories;

import java.util.List;

import com.assamadda.models.MasterDistrict;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MasterDistrictRepository extends JpaRepository<MasterDistrict, Long>{

	List<MasterDistrict> findByIsActiveOrderByName(boolean b);

}