package com.assamadda.repositories;

import java.util.Date;
import java.util.List;

import com.assamadda.models.TransArticle;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransArticleRepository extends JpaRepository<TransArticle, Long>{

	List<TransArticle> findByCreatedAtBetweenAndIsActiveOrderByCreatedAtDesc(Date startDate, Date endDate, boolean b);

	TransArticle findBySlugAndIsActive(String articleSlug, boolean b);

	Page<TransArticle> findByIsActiveAndVisibleTo(boolean b, String visibleTo, Pageable sortedByCapturedAtDesc);

	TransArticle findBySlugAndVisibleToAndIsActive(String slug, String string, boolean b);

	List<TransArticle> findByIsActiveOrderByCreatedAtDesc(boolean b);


}