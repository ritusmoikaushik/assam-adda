package com.assamadda.repositories;

import java.util.List;

import com.assamadda.models.MasterQualification;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MasterQualificationRepository extends JpaRepository<MasterQualification, Long>{

	List<MasterQualification> findByIsActiveOrderByName(boolean b);

}