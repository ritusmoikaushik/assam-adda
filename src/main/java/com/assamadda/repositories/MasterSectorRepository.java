package com.assamadda.repositories;

import java.util.List;

import com.assamadda.models.MasterSector;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MasterSectorRepository extends JpaRepository<MasterSector, Long>{

	List<MasterSector> findByIsActiveOrderByName(boolean b);

}