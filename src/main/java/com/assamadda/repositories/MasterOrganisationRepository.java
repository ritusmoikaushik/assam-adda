package com.assamadda.repositories;

import java.util.List;

import com.assamadda.models.MasterOrganisation;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MasterOrganisationRepository extends JpaRepository<MasterOrganisation, Long>{

	List<MasterOrganisation> findByNameContainingAndIsActive(String searchText, boolean b);

}