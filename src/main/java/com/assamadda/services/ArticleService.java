package com.assamadda.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import com.assamadda.models.TransArticle;
import com.assamadda.payloads.ArticlePayload;
import com.assamadda.payloads.JsonResponse;
import com.assamadda.payloads.PageableObjectPayload;
import com.assamadda.repositories.TransArticleRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * ArticleService
 */
@Service
public class ArticleService {
   
    @Autowired
    TransArticleRepository transArticleRepository;

    // ========================================================================
    // # PAGE CREATE ARTICLE
    // ========================================================================
	public PageableObjectPayload getAllObjectsForCreatingArticle() {
        PageableObjectPayload articleData = new PageableObjectPayload();
		return articleData;
	}

    // ========================================================================
    // # METHOD TO CREATE/UPDATE NEW ARTICLE
    // ========================================================================
	public JsonResponse createArticle(@Valid TransArticle transArticle) {
		JsonResponse res = new JsonResponse();
        transArticleRepository.save(transArticle);
        String slug = transArticle.getTitle().trim().toLowerCase().replaceAll("[^a-zA-Z0-9]", " ").trim().replace(" ", "-") + "-"
                + String.valueOf(transArticle.getArticleId());
                transArticle.setSlug(slug);
                transArticleRepository.save(transArticle);
        res.setResult(true);
        return res;
	}

    // ========================================================================
    // # GET ARTICLE LIST BY DATE RANGE
    // ========================================================================
	public JsonResponse getArticleList() {
        JsonResponse res = new JsonResponse();
        PageableObjectPayload articleData = new PageableObjectPayload();
        List<ArticlePayload> allArticleDetails = new ArrayList<>();
        ArticlePayload articleDetail = null;

        List<TransArticle> articleList = transArticleRepository.findByIsActiveOrderByCreatedAtDesc(true);

        for(TransArticle article : articleList){
            articleDetail = new ArticlePayload();

            articleDetail.setArticleId(article.getArticleId());
            articleDetail.setTitle(article.getTitle());
            articleDetail.setDescription(article.getDescription());
            articleDetail.setCreatedAt(article.getCreatedAt());
            articleDetail.setVisibleTo(article.getVisibleTo());
            articleDetail.setSlug(article.getSlug());

            allArticleDetails.add(articleDetail);
        }


        articleData.setObjectList(allArticleDetails);
        res.setPayload(articleData);
        res.setResult(true);
		return res;
	}

    // ========================================================================
    // # DELETE ARTICLE
    // ========================================================================
	public JsonResponse deleteArticle(Long articleId) {
		JsonResponse res = new JsonResponse();
        TransArticle transArticle = transArticleRepository.getOne(articleId);
        transArticle.setIsActive(false);
        transArticleRepository.save(transArticle);
        res.setResult(true);
        return res;
	}

    // ========================================================================
    // # GET ARTICLE DETAILS BY SLUG
    // ========================================================================
	public JsonResponse getArticleArticleDataBySlug(String articleSlug) {
        JsonResponse res = new JsonResponse();

        TransArticle transArticle = transArticleRepository.findBySlugAndIsActive(articleSlug,true);
        if(!Objects.equals(transArticle, null)){
            res.setPayload(transArticle);
            res.setMessage("Article Data fetched successfully");
            res.setResult(true);
        }
        return res;
	}

	
}