package com.assamadda.services;

import java.net.URI;
import java.util.regex.Pattern;

import com.assamadda.config.CaptchaSettings;
import com.assamadda.payloads.ReCaptchaResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

@Service
public class CaptchaService{ 
 
    @Autowired
    private CaptchaSettings captchaSettings;
 
    @Autowired
    private RestTemplate restTemplate;
 
    private static Pattern RESPONSE_PATTERN = Pattern.compile("[A-Za-z0-9_-]+");
 
    public String processResponse(String response) {
        if(!responseSanityCheck(response)) {
            return "Recaptcha Error";
        }
 
        URI verifyUri = URI.create(String.format(
          "https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s",
          captchaSettings.getSecret(), response));
 
          ReCaptchaResponse recaptchaResponse = restTemplate.getForObject(verifyUri, ReCaptchaResponse.class);
 
        if(!recaptchaResponse.isSuccess()) {
            return "Recaptcha Error";
        }

        if(recaptchaResponse.isSuccess()) {
            return "success";
        }

        return "Recaptcha Error";
    }
 
    private boolean responseSanityCheck(String response) {
        return StringUtils.hasLength(response) && RESPONSE_PATTERN.matcher(response).matches();
    }


    @Bean
public RestTemplate restTemplate() {
    return new RestTemplate();
}
}