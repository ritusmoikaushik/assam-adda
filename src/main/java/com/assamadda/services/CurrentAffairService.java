package com.assamadda.services;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import com.assamadda.models.TransCurrentAffair;
import com.assamadda.payloads.JsonResponse;
import com.assamadda.payloads.PageDetails;
import com.assamadda.payloads.PageableObjectPayload;
import com.assamadda.repositories.TransCurrentAffairRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class CurrentAffairService {
    

    @Autowired
    TransCurrentAffairRepository transCurrentAffairRepository;

	public JsonResponse createCurrentAffair(@Valid TransCurrentAffair transCurrentAffair) {
		JsonResponse res = new JsonResponse();
        transCurrentAffairRepository.save(transCurrentAffair);
        res.setResult(true);
        return res;
	}

	public JsonResponse getCaListByDateRange(Date startDate, Date endDate) {
		JsonResponse res = new JsonResponse();
        PageableObjectPayload caData = new PageableObjectPayload();

        List<TransCurrentAffair> caList = transCurrentAffairRepository.findByPostDateBetweenAndIsActiveOrderByPostDateDesc(startDate, endDate, true);

        caData.setObjectList(caList);
        res.setPayload(caData);
        res.setResult(true);
		return res;
	}

	public JsonResponse deleteCA(Long caId) {
		JsonResponse res = new JsonResponse();
        TransCurrentAffair transCurrentAffair = transCurrentAffairRepository.getOne(caId);
        transCurrentAffair.setIsActive(false);
        transCurrentAffairRepository.save(transCurrentAffair);
        res.setResult(true);
        return res;
	}

	public JsonResponse getCAById(Long caId) {
        JsonResponse res = new JsonResponse();
        TransCurrentAffair transCurrentAffair = transCurrentAffairRepository.getOne(caId);
        res.setPayload(transCurrentAffair);
        res.setResult(true);
        return res;	
    }

	public PageableObjectPayload getPageablePublicLatestCA(int pageNo) {
		PageableObjectPayload caData = new PageableObjectPayload();

        Pageable sortedByPostDesc = PageRequest.of(pageNo, 50, Sort.by("postDate").descending());
                
        Page<TransCurrentAffair> caList = transCurrentAffairRepository.findByIsActiveAndVisibleTo(true, "Public", sortedByPostDesc);

        

        caData.setPageDetails(new PageDetails(caList.getPageable().getPageNumber(),
        caList.getNumberOfElements(), caList.getPageable().getPageSize(),
        caList.hasPrevious(), caList.hasNext(), caList.getTotalElements(),
        caList.getTotalPages(),caList.getPageable().getPageNumber()+1));

        caData.setObjectList(caList.getContent());

        return caData;
	}
}