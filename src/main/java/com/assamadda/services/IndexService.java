package com.assamadda.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.assamadda.models.MasterOrganisation;
import com.assamadda.models.MasterQualification;
import com.assamadda.models.TransArticle;
import com.assamadda.models.TransJob;
import com.assamadda.models.TransSubscribe;
import com.assamadda.payloads.JobPayload;
import com.assamadda.payloads.JsonResponse;
import com.assamadda.payloads.PageDetails;
import com.assamadda.payloads.PageableObjectPayload;
import com.assamadda.repositories.MasterOrganisationRepository;
import com.assamadda.repositories.MasterQualificationRepository;
import com.assamadda.repositories.TransArticleRepository;
import com.assamadda.repositories.TransJobRepository;
import com.assamadda.repositories.TransSubscribeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class IndexService {
    @Autowired
    TransJobRepository transJobRepository;

    @Autowired
    TransArticleRepository transArticleRepository;

    @Autowired
    TransSubscribeRepository transSubscribeRepository;

    @Autowired
    MasterOrganisationRepository masterOrganisationRepository;

    @Autowired
    MasterQualificationRepository masterQualificationRepository;

    // ========================================================================
    // # PUBLIC INDEX PAGE
    // ========================================================================
	public PageableObjectPayload getPageableJobsForIndexPage(int pageNo) {
		PageableObjectPayload jobData = new PageableObjectPayload();
        List<JobPayload> allJobDetails = new ArrayList<>();
        JobPayload jobDetail = null;

        Pageable sortedByPostDesc = PageRequest.of(pageNo, 10, Sort.by("postDate").descending());
                
        Page<TransJob> jobList = transJobRepository.findByIsActiveAndVisibleTo(true, "Public", sortedByPostDesc);

        MasterOrganisation org = null;
        byte[] emptyArray = new byte[0];

        for(TransJob job : jobList.getContent()){
            jobDetail = new JobPayload();

            jobDetail.setJobId(job.getJobId());
            jobDetail.setTitle(job.getTitle());
            jobDetail.setDescription(job.getDescription());
            jobDetail.setCreatedAt(job.getCreatedAt());
            jobDetail.setPostDate(job.getPostDate());
            jobDetail.setSlug(job.getSlug());
            jobDetail.setCategory(job.getCategory());

            if(!Objects.equals(job.getOrganisation(), null)){
                org = job.getOrganisation();
                org.setImage(emptyArray);
    
                jobDetail.setOrganisation(org);
            }

            allJobDetails.add(jobDetail);
        }

        jobData.setPageDetails(new PageDetails(jobList.getPageable().getPageNumber(),
        jobList.getNumberOfElements(), jobList.getPageable().getPageSize(),
        jobList.hasPrevious(), jobList.hasNext(), jobList.getTotalElements(),
        jobList.getTotalPages(),jobList.getPageable().getPageNumber()+1));

        jobData.setObjectList(allJobDetails);

        return jobData;
	}

    // ========================================================================
    // # GET ORGANISATION IMAGE
    // ========================================================================
	public MasterOrganisation getOrganisation(Long organisationId) {
		return masterOrganisationRepository.getOne(organisationId);
	}

    // ========================================================================
    // # PUBLIC JOB DETAILS
    // ========================================================================
	public PageableObjectPayload getJobDetailsBySlug(String slug) {
		PageableObjectPayload jobData = new PageableObjectPayload();
        JobPayload jobDetail = null;
        MasterOrganisation org = null;

        TransJob job = transJobRepository.findBySlugAndVisibleToAndIsActive(slug, "Public",true);
        if(!Objects.equals(job, null)){
            jobDetail = new JobPayload();

            jobDetail.setJobId(job.getJobId());
            jobDetail.setTitle(job.getTitle());
            jobDetail.setDescription(job.getDescription());
            jobDetail.setCreatedAt(job.getCreatedAt());            
            jobDetail.setPostDate(job.getPostDate());
            jobDetail.setSlug(job.getSlug());
            jobDetail.setCategory(job.getCategory());
            jobDetail.setContent(job.getContent());
            jobDetail.setDistricts(job.getDistricts());
            jobDetail.setSectors(job.getSectors());

            org = job.getOrganisation();
            org.setImage(null);

            jobDetail.setOrganisation(org);

        }
        jobData.setCurrentObject(jobDetail);
        return jobData;
	}

    // ========================================================================
    // # PUBLIC JOB SEARCH PAGE
    // ========================================================================
	public PageableObjectPayload getSearchedJobs(String searchText, int pageNo) {
		PageableObjectPayload jobData = new PageableObjectPayload();
        List<JobPayload> allJobDetails = new ArrayList<>();
        JobPayload jobDetail = null;

        Pageable sortedByPostDesc = PageRequest.of(pageNo, 10, Sort.by("postDate").descending());
                
        Page<TransJob> jobList = transJobRepository.findByTitleContainingAndIsActiveAndVisibleTo(searchText, true, "Public", sortedByPostDesc);

        MasterOrganisation org = null;

        for(TransJob job : jobList.getContent()){
            jobDetail = new JobPayload();

            jobDetail.setJobId(job.getJobId());
            jobDetail.setTitle(job.getTitle());
            jobDetail.setDescription(job.getDescription());
            jobDetail.setCreatedAt(job.getCreatedAt());           
            jobDetail.setPostDate(job.getPostDate());
            jobDetail.setSlug(job.getSlug());
            jobDetail.setCategory(job.getCategory());

            org = job.getOrganisation();
            org.setImage(null);

            jobDetail.setOrganisation(org);

            allJobDetails.add(jobDetail);
        }

        jobData.setPageDetails(new PageDetails(jobList.getPageable().getPageNumber(),
        jobList.getNumberOfElements(), jobList.getPageable().getPageSize(),
        jobList.hasPrevious(), jobList.hasNext(), jobList.getTotalElements(),
        jobList.getTotalPages(),jobList.getPageable().getPageNumber()+1));


        jobData.setObjectList(allJobDetails);

        return jobData;
	}

    // ========================================================================
    // # PUBLIC JOB BY DISTRICT PAGE
    // ========================================================================
	public PageableObjectPayload getPageableJobsByDistrict(String district, int pageNo) {
		PageableObjectPayload jobData = new PageableObjectPayload();
        List<JobPayload> allJobDetails = new ArrayList<>();
        JobPayload jobDetail = null;

        Pageable sortedByPostDesc = PageRequest.of(pageNo, 10, Sort.by("postDate").descending());
                
        Page<TransJob> jobList = transJobRepository.findByDistricts_descriptionAndIsActiveAndVisibleTo(district, true, "Public", sortedByPostDesc);

        MasterOrganisation org = null;

        for(TransJob job : jobList.getContent()){
            jobDetail = new JobPayload();

            jobDetail.setJobId(job.getJobId());
            jobDetail.setTitle(job.getTitle());
            jobDetail.setDescription(job.getDescription());
            jobDetail.setCreatedAt(job.getCreatedAt());           
            jobDetail.setPostDate(job.getPostDate());
            jobDetail.setSlug(job.getSlug());
            jobDetail.setCategory(job.getCategory());

            org = job.getOrganisation();
            org.setImage(null);

            jobDetail.setOrganisation(org);

            allJobDetails.add(jobDetail);
        }

        jobData.setPageDetails(new PageDetails(jobList.getPageable().getPageNumber(),
        jobList.getNumberOfElements(), jobList.getPageable().getPageSize(),
        jobList.hasPrevious(), jobList.hasNext(), jobList.getTotalElements(),
        jobList.getTotalPages(),jobList.getPageable().getPageNumber()+1));


        jobData.setObjectList(allJobDetails);

        return jobData;
	}
    
// ========================================================================
    // # PUBLIC JOB BY SECTOR PAGE
    // ========================================================================
	public PageableObjectPayload getPageableJobsBySector(String sector, int pageNo) {
		PageableObjectPayload jobData = new PageableObjectPayload();
        List<JobPayload> allJobDetails = new ArrayList<>();
        JobPayload jobDetail = null;

        Pageable sortedByPostDesc = PageRequest.of(pageNo, 10, Sort.by("postDate").descending());
                
        Page<TransJob> jobList = transJobRepository.findBySectors_descriptionAndIsActiveAndVisibleTo(sector, true, "Public", sortedByPostDesc);

        MasterOrganisation org = null;

        for(TransJob job : jobList.getContent()){
            jobDetail = new JobPayload();

            jobDetail.setJobId(job.getJobId());
            jobDetail.setTitle(job.getTitle());
            jobDetail.setDescription(job.getDescription());
            jobDetail.setCreatedAt(job.getCreatedAt());           
            jobDetail.setPostDate(job.getPostDate());
            jobDetail.setSlug(job.getSlug());
            jobDetail.setCategory(job.getCategory());

            org = job.getOrganisation();
            org.setImage(null);

            jobDetail.setOrganisation(org);

            allJobDetails.add(jobDetail);
        }

        jobData.setPageDetails(new PageDetails(jobList.getPageable().getPageNumber(),
        jobList.getNumberOfElements(), jobList.getPageable().getPageSize(),
        jobList.hasPrevious(), jobList.hasNext(), jobList.getTotalElements(),
        jobList.getTotalPages(),jobList.getPageable().getPageNumber()+1));


        jobData.setObjectList(allJobDetails);

        return jobData;
    }

    // ========================================================================
    // # PUBLIC JOB BY QUALIFICATION PAGE
    // ========================================================================
	public PageableObjectPayload getPageableJobsByQualification(String qualification, int pageNo) {
		PageableObjectPayload jobData = new PageableObjectPayload();
        List<JobPayload> allJobDetails = new ArrayList<>();
        JobPayload jobDetail = null;

        Pageable sortedByPostDesc = PageRequest.of(pageNo, 10, Sort.by("postDate").descending());
                
        Page<TransJob> jobList = transJobRepository.findByQualifications_slugAndIsActiveAndVisibleTo(qualification, true, "Public", sortedByPostDesc);

        MasterOrganisation org = null;

        for(TransJob job : jobList.getContent()){
            jobDetail = new JobPayload();

            jobDetail.setJobId(job.getJobId());
            jobDetail.setTitle(job.getTitle());
            jobDetail.setDescription(job.getDescription());
            jobDetail.setCreatedAt(job.getCreatedAt());           
            jobDetail.setPostDate(job.getPostDate());
            jobDetail.setSlug(job.getSlug());
            jobDetail.setCategory(job.getCategory());

            org = job.getOrganisation();
            org.setImage(null);

            jobDetail.setOrganisation(org);

            allJobDetails.add(jobDetail);
        }

        jobData.setPageDetails(new PageDetails(jobList.getPageable().getPageNumber(),
        jobList.getNumberOfElements(), jobList.getPageable().getPageSize(),
        jobList.hasPrevious(), jobList.hasNext(), jobList.getTotalElements(),
        jobList.getTotalPages(),jobList.getPageable().getPageNumber()+1));


        jobData.setObjectList(allJobDetails);

        return jobData;
    }
    
    // ========================================================================
    // # PUBLIC JOB BY CATEGORY PAGE
    // ========================================================================
	public PageableObjectPayload getPageableJobsByCategory(String category, int pageNo) {
		PageableObjectPayload jobData = new PageableObjectPayload();
        List<JobPayload> allJobDetails = new ArrayList<>();
        JobPayload jobDetail = null;

        Pageable sortedByPostDesc = PageRequest.of(pageNo, 10, Sort.by("postDate").descending());
                
        Page<TransJob> jobList = transJobRepository.findByCategoryAndIsActiveAndVisibleTo(category, true, "Public", sortedByPostDesc);

        MasterOrganisation org = null;

        for(TransJob job : jobList.getContent()){
            jobDetail = new JobPayload();

            jobDetail.setJobId(job.getJobId());
            jobDetail.setTitle(job.getTitle());
            jobDetail.setDescription(job.getDescription());
            jobDetail.setCreatedAt(job.getCreatedAt());           
            jobDetail.setPostDate(job.getPostDate());
            jobDetail.setSlug(job.getSlug());
            jobDetail.setCategory(job.getCategory());

            org = job.getOrganisation();
            org.setImage(null);

            jobDetail.setOrganisation(org);

            allJobDetails.add(jobDetail);
        }

        jobData.setPageDetails(new PageDetails(jobList.getPageable().getPageNumber(),
        jobList.getNumberOfElements(), jobList.getPageable().getPageSize(),
        jobList.hasPrevious(), jobList.hasNext(), jobList.getTotalElements(),
        jobList.getTotalPages(),jobList.getPageable().getPageNumber()+1));


        jobData.setObjectList(allJobDetails);

        return jobData;
	}

	public TransSubscribe findSubscriberByEmail(String email) {
		return transSubscribeRepository.findByEmail(email);
	}

	public JsonResponse saveSubscriber(TransSubscribe transSubscribe) {
        JsonResponse res = new JsonResponse();
        try{
            transSubscribeRepository.save(transSubscribe);
            res.setResult(true);
            res.setMessage("Subscribed successfully.");
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return res;
	}

	public List<MasterQualification> getQualificationList() {
		return masterQualificationRepository.findByIsActiveOrderByName(true);
	}

	public PageableObjectPayload getArticleDetailsBySlug(String slug) {
		PageableObjectPayload jobData = new PageableObjectPayload();
        
        TransArticle transArticle = transArticleRepository.findBySlugAndVisibleToAndIsActive(slug, "Public",true);
        
        jobData.setCurrentObject(transArticle);
        return jobData;
	}
}