package com.assamadda.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import com.assamadda.models.MasterDistrict;
import com.assamadda.models.MasterOrganisation;
import com.assamadda.models.MasterQualification;
import com.assamadda.models.MasterSector;
import com.assamadda.models.TransJob;
import com.assamadda.payloads.JobPayload;
import com.assamadda.payloads.JsonResponse;
import com.assamadda.payloads.PageableObjectPayload;
import com.assamadda.repositories.MasterDistrictRepository;
import com.assamadda.repositories.MasterOrganisationRepository;
import com.assamadda.repositories.MasterQualificationRepository;
import com.assamadda.repositories.MasterSectorRepository;
import com.assamadda.repositories.TransJobRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * JobService
 */
@Service
public class JobService {
   
    @Autowired
    TransJobRepository transJobRepository;

    @Autowired
    MasterDistrictRepository masterDistrictRepository;

    @Autowired
    MasterQualificationRepository masterQualificationRepository;

    @Autowired
    MasterSectorRepository masterSectorRepository;

    @Autowired
    MasterOrganisationRepository masterOrganisationRepository;

    // ========================================================================
    // # PAGE CREATE JOB
    // ========================================================================
	public PageableObjectPayload getAllObjectsForCreatingJob() {
        PageableObjectPayload jobData = new PageableObjectPayload();

        List<MasterDistrict> districtList =  masterDistrictRepository.findByIsActiveOrderByName(true);
        List<MasterQualification> qualificationList =  masterQualificationRepository.findByIsActiveOrderByName(true);
        List<MasterSector> sectorList =  masterSectorRepository.findByIsActiveOrderByName(true);

        jobData.setObjectList(districtList);
        jobData.setObjectList2(sectorList);
        jobData.setObjectList3(qualificationList);

		return jobData;
	}

    // ========================================================================
    // # METHOD TO CREATE/UPDATE NEW JOB
    // ========================================================================
	public JsonResponse createJob(@Valid TransJob transJob) {
		JsonResponse res = new JsonResponse();
        transJobRepository.save(transJob);
        String slug = transJob.getTitle().trim().toLowerCase().replaceAll("[^a-zA-Z0-9]", " ").trim().replace(" ", "-") + "-"
                + String.valueOf(transJob.getJobId());
                transJob.setSlug(slug);
                transJobRepository.save(transJob);
        res.setResult(true);
        return res;
	}

    // ========================================================================
    // # GET JOB LIST BY DATE RANGE
    // ========================================================================
	public JsonResponse getJobListByDateRange(Date startDate, Date endDate) {
        JsonResponse res = new JsonResponse();
        PageableObjectPayload jobData = new PageableObjectPayload();
        List<JobPayload> allJobDetails = new ArrayList<>();
        JobPayload jobDetail = null;

        List<TransJob> jobList = transJobRepository.findByCreatedAtBetweenAndIsActiveOrderByCreatedAtDesc(startDate, endDate, true);

        for(TransJob job : jobList){
            jobDetail = new JobPayload();

            jobDetail.setJobId(job.getJobId());
            jobDetail.setTitle(job.getTitle());
            jobDetail.setDescription(job.getDescription());
            jobDetail.setCreatedAt(job.getCreatedAt());
            jobDetail.setVisibleTo(job.getVisibleTo());
            jobDetail.setSlug(job.getSlug());

            allJobDetails.add(jobDetail);
        }


        jobData.setObjectList(allJobDetails);
        res.setPayload(jobData);
        res.setResult(true);
		return res;
	}

    // ========================================================================
    // # DELETE JOB
    // ========================================================================
	public JsonResponse deleteJob(Long jobId) {
		JsonResponse res = new JsonResponse();
        TransJob transJob = transJobRepository.getOne(jobId);
        transJob.setIsActive(false);
        transJobRepository.save(transJob);
        res.setResult(true);
        return res;
	}

    // ========================================================================
    // # GET JOB DETAILS BY SLUG
    // ========================================================================
	public JsonResponse getJobJobDataBySlug(String jobSlug) {
        JsonResponse res = new JsonResponse();

        TransJob transJob = transJobRepository.findBySlugAndIsActive(jobSlug,true);
        if(!Objects.equals(transJob, null)){
            res.setPayload(transJob);
            res.setMessage("Job Data fetched successfully");
            res.setResult(true);
        }
        return res;
	}

	public JsonResponse getSearchOrganisation(String searchText) {
        JsonResponse res = new JsonResponse();
        PageableObjectPayload orgData = new PageableObjectPayload();

        List<MasterOrganisation> organisationList =  masterOrganisationRepository.findByNameContainingAndIsActive(searchText,true);
        orgData.setObjectList(organisationList);
        res.setPayload(orgData);
        res.setResult(true);
        
        return res;
	}

    // ========================================================================
    // # METHOD TO CREATE NEW ORGANISATION
    // ========================================================================
	public JsonResponse createOrganisation(@Valid MasterOrganisation masterOrganisation) {
		JsonResponse res = new JsonResponse();
        masterOrganisationRepository.save(masterOrganisation);
        res.setResult(true);
        return res;
	}

    
}