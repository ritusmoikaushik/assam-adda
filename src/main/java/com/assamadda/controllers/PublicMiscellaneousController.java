package com.assamadda.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/")
public class PublicMiscellaneousController {
    
    // ========================================================================
    // # PUBLIC CONTACT US PAGE
    // ========================================================================
    @GetMapping(value = "contact-us")
    public ModelAndView contactUs(ModelAndView mv) {
        mv = new ModelAndView("unlogged/contact_us");
        return mv;
    }

    // ========================================================================
    // # PUBLIC ABOUT PAGE
    // ========================================================================
    @GetMapping(value = "about")
    public ModelAndView about(ModelAndView mv) {
        mv = new ModelAndView("unlogged/about");
        return mv;
    }

    // ========================================================================
    // # PUBLIC SUBSCRIBE PAGE
    // ========================================================================
    @GetMapping(value = "subscribe")
    public ModelAndView subscribe(ModelAndView mv) {
        mv = new ModelAndView("unlogged/subscribe");
        return mv;
    }

    // ========================================================================
    // # PUBLIC POST A JOB PAGE
    // ========================================================================
    @GetMapping(value = "post-job")
    public ModelAndView postJob(ModelAndView mv) {
        mv = new ModelAndView("unlogged/post_job");
        return mv;
    }


    // ========================================================================
    // # PUBLIC ASSAM GK PAGE
    // ========================================================================
    @GetMapping(value = "assam-general-knowledge")
    public ModelAndView assamGeneralKnowledge(ModelAndView mv) {
        mv = new ModelAndView("unlogged/assam_general_knowledge");
        return mv;
    }

    // ========================================================================
    // # PUBLIC PRIVACY POLICY PAGE
    // ========================================================================
    @GetMapping(value = "privacy-policy")
    public ModelAndView privacyPolicy(ModelAndView mv) {
        mv = new ModelAndView("unlogged/privacy_policy");
        return mv;
    }

    // ========================================================================
    // # PUBLIC TERMS OF SERVICE PAGE
    // ========================================================================
    @GetMapping(value = "terms-of-service")
    public ModelAndView termsOfService(ModelAndView mv) {
        mv = new ModelAndView("unlogged/terms_of_service");
        return mv;
    }

    // ========================================================================
    // # PUBLIC RESUME PAGE
    // ========================================================================
    @GetMapping(value = "resume")
    public ModelAndView resume(ModelAndView mv) {
        mv = new ModelAndView("unlogged/resume");
        return mv;
    }

    // ========================================================================
    // # PUBLIC BOOKS PAGE
    // ========================================================================
    @GetMapping(value = "books")
    public ModelAndView books(ModelAndView mv) {
        mv = new ModelAndView("unlogged/books");
        return mv;
    }

}