package com.assamadda.controllers;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * ContributorController
 */
@RestController
@RequestMapping("/contributor")
public class ContributorController {

    

    // ========================================================================
    // # PAGE CONTRIBUTOR HOME
    // ========================================================================
    @GetMapping("")
    public ModelAndView pageContributorHome(ModelAndView mv) {
        mv = new ModelAndView("contributor/home");
        return mv;
    }

}