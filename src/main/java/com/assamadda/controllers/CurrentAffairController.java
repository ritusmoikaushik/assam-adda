package com.assamadda.controllers;

import java.util.Date;
import java.util.Objects;
import java.util.Optional;

import javax.validation.Valid;

import com.assamadda.exception.BadRequestException;
import com.assamadda.models.TransCurrentAffair;
import com.assamadda.payloads.JsonResponse;
import com.assamadda.payloads.PageableObjectPayload;
import com.assamadda.services.CurrentAffairService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class CurrentAffairController {
    
    @Autowired
    CurrentAffairService currentAffairService;

    // ========================================================================
    // # CONTRIBUTOR PAGE CURRENT AFFAIR
    // ========================================================================
    @GetMapping(value = { "/contributor/current-affair" })
    public ModelAndView pageContributorCurrentAffairHome(ModelAndView mv) {
        mv = new ModelAndView("contributor/current-affair");
        return mv;
    }

    // ========================================================================
    // # METHOD TO CREATE/UPDATE NEW CA
    // ========================================================================
    @PostMapping(value = { "/contributor/create-current-affair" })
    public ResponseEntity<JsonResponse> createCurrentAffair(@Valid @ModelAttribute TransCurrentAffair transCurrentAffair, BindingResult bindingResult)
            throws BindException {
        Long jobId = transCurrentAffair.getCaId();

        if (!bindingResult.hasErrors()) {
            JsonResponse res = currentAffairService.createCurrentAffair(transCurrentAffair);

            if (Objects.equals(res.getResult(), true)) {
                if (Objects.equals(jobId, null)) {
                    res.setMessage("CA Created Successfully.");
                } else {
                    res.setMessage("CA Updated Successfully.");
                }
                return ResponseEntity.ok().body(res);
            } else {
                throw new BadRequestException("Operation Failed");
            }
        } else {
            throw new BindException(bindingResult);
        }
    }

    // ========================================================================
    // # GET CA LIST BY DATE RANGE
    // ========================================================================
    @GetMapping(value = { "/contributor/get-contributor-current-affair-list-daterange" })
    public ResponseEntity<JsonResponse> getCaListByDateRange(@RequestParam(value = "startDate") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date startDate,
                                                                @RequestParam(value = "endDate") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date endDate) {

        JsonResponse res = currentAffairService.getCaListByDateRange(startDate, endDate);

        if (Objects.equals(res.getResult(), true)) {
            return ResponseEntity.ok().body(res);
        } else {
            throw new BadRequestException("Operation Failed");
        }
    }

    // ========================================================================
    // # DELETE CA
    // ========================================================================
    @PutMapping(value = { "/contributor/delete-current-affair" })
    public ResponseEntity<JsonResponse> deleteCA(@RequestParam("caId") Long caId) {

        JsonResponse res = currentAffairService.deleteCA(caId);

        if (Objects.equals(res.getResult(), true)) {
            res.setMessage("CA Deleted Successfully.");
            return ResponseEntity.ok().body(res);
        } else {
            throw new BadRequestException("Operation Failed");
        }
    }

    // ========================================================================
    // # GET CA DETAILS By ID
    // ========================================================================
    @GetMapping(value = { "/contributor/get-contributor-current-affair-data" })
    public ResponseEntity<JsonResponse> getJobJobDataBySlug(@RequestParam(value = "caId") Long caId) {

        JsonResponse res = currentAffairService.getCAById(caId);

        if (Objects.equals(res.getResult(), true)) {
            return ResponseEntity.ok().body(res);
        } else {
            throw new BadRequestException("Operation Failed");
        }
    }

    // ========================================================================
    // # PUBLIC CURRENT AFFAIRS
    // ========================================================================
    @GetMapping(value = "/latest-current-affairs")
    public ModelAndView getIndex(ModelAndView mv, @RequestParam("pageNo") Optional<Integer> pageNo) {
        mv = new ModelAndView("unlogged/current_affair");
        int page = pageNo.orElse(0);

        PageableObjectPayload caData = currentAffairService.getPageablePublicLatestCA(page);
        mv.addObject("caData", caData);

        return mv;
    }

}

