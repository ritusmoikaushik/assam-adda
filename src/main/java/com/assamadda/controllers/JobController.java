package com.assamadda.controllers;

import java.util.Date;
import java.util.Objects;

import javax.validation.Valid;

import com.assamadda.exception.BadRequestException;
import com.assamadda.models.MasterOrganisation;
import com.assamadda.models.TransJob;
import com.assamadda.payloads.JsonResponse;
import com.assamadda.payloads.PageableObjectPayload;
import com.assamadda.services.JobService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;

/**
 * JobController
 */
@RestController
@RequestMapping("/contributor")
public class JobController {

    @Autowired
    JobService jobService;

    // ========================================================================
    // # PAGE JOBS
    // ========================================================================
    @GetMapping(value = { "/jobs" })
    public ModelAndView pageJobJobs(ModelAndView mv) {
        mv = new ModelAndView("contributor/jobs");
        return mv;
    }

    // ========================================================================
    // # PAGE CREATE JOB
    // ========================================================================
    @GetMapping(value = { "/jobs/create" })
    public ModelAndView createJob(ModelAndView mv) {
        mv = new ModelAndView("contributor/create_update_job");

        PageableObjectPayload jobData = jobService.getAllObjectsForCreatingJob();
        mv.addObject("jobData", jobData);

        mv.addObject("pagePurpose", "CREATE JOB");
        return mv;
    }

    // ========================================================================
    // # METHOD TO CREATE/UPDATE NEW ARTICLE
    // ========================================================================
    @PostMapping(value = { "/create-job" })
    public ResponseEntity<JsonResponse> createJob(@Valid @ModelAttribute TransJob transJob, BindingResult bindingResult)
            throws BindException {
        Long jobId = transJob.getJobId();

        if ((Objects.equals(transJob.getOrganisation(), null))) {
            bindingResult.rejectValue("organisation", "error.organisation", "O bondhu... Muk napahoriba he..");
        }

        if (!bindingResult.hasErrors()) {
            JsonResponse res = jobService.createJob(transJob);

            if (Objects.equals(res.getResult(), true)) {
                if (Objects.equals(jobId, null)) {
                    res.setMessage("Job Created Successfully.");
                } else {
                    res.setMessage("Job Updated Successfully.");
                }
                return ResponseEntity.ok().body(res);
            } else {
                throw new BadRequestException("Operation Failed");
            }
        } else {
            throw new BindException(bindingResult);
        }
    }

    // ========================================================================
    // # METHOD TO CREATE NEW ORGANISATION
    // ========================================================================
    @PostMapping(value = { "/create-organisation" })
    public ResponseEntity<JsonResponse> createOrganisation(@Valid @ModelAttribute MasterOrganisation masterOrganisation) {
        Long organisationIdId = masterOrganisation.getOrganisationId();

        JsonResponse res = jobService.createOrganisation(masterOrganisation);

        if (Objects.equals(res.getResult(), true)) {
            if (Objects.equals(organisationIdId, null)) {
                res.setMessage("Organisation Created Successfully.");
            } else {
                res.setMessage("Organisation Updated Successfully.");
            }
            return ResponseEntity.ok().body(res);
        } else {
            throw new BadRequestException("Operation Failed");
        }
    }

    

    // ========================================================================
    // # GET JOB LIST BY DATE RANGE
    // ========================================================================
    @GetMapping(value = { "/get-contributor-job-list-daterange" })
    public ResponseEntity<JsonResponse> getJobListByDateRange(@RequestParam(value = "startDate") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date startDate,
                                                                @RequestParam(value = "endDate") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date endDate) {

        JsonResponse res = jobService.getJobListByDateRange(startDate, endDate);

        if (Objects.equals(res.getResult(), true)) {
            return ResponseEntity.ok().body(res);
        } else {
            throw new BadRequestException("Operation Failed");
        }
    }

    // ========================================================================
    // # PAGE UPDATE JOB
    // ========================================================================
    @GetMapping(value = { "/jobs/update/{jobSlug}" })
    public ModelAndView updateJob(@PathVariable("jobSlug") String jobSlug, ModelAndView mv) {
        mv = new ModelAndView("contributor/create_update_job");

        PageableObjectPayload jobData = jobService.getAllObjectsForCreatingJob();
        mv.addObject("jobData", jobData);

        mv.addObject("jobSlug", jobSlug);
        mv.addObject("pagePurpose", "UPDATE JOB");
        return mv;
    }

    // ========================================================================
    // # DELETE JOB
    // ========================================================================
    @PutMapping(value = { "/delete-job" })
    public ResponseEntity<JsonResponse> deleteJob(@RequestParam("jobId") Long jobId) {

        JsonResponse res = jobService.deleteJob(jobId);

        if (Objects.equals(res.getResult(), true)) {
            res.setMessage("Job Deleted Successfully.");
            return ResponseEntity.ok().body(res);
        } else {
            throw new BadRequestException("Operation Failed");
        }
    }

    // ========================================================================
    // # GET JOB DETAILS By SLUG
    // ========================================================================
    @GetMapping(value = { "/get-contributor-job-data" })
    public ResponseEntity<JsonResponse> getJobJobDataBySlug(@RequestParam(value = "jobSlug") String jobSlug) {

        JsonResponse res = jobService.getJobJobDataBySlug(jobSlug);

        if (Objects.equals(res.getResult(), true)) {
            return ResponseEntity.ok().body(res);
        } else {
            throw new BadRequestException("Operation Failed");
        }
    }

    // ========================================================================
    // # GET SEARCHED ORGANISATION
    // ========================================================================
    @GetMapping(value = { "/get-search-organisation" })
    public ResponseEntity<JsonResponse> getSearchOrganisation(@RequestParam(value = "searchText") String searchText) {

        JsonResponse res = jobService.getSearchOrganisation(searchText);

        if (Objects.equals(res.getResult(), true)) {
            return ResponseEntity.ok().body(res);
        } else {
            throw new BadRequestException("Operation Failed");
        }
    }

    // ========================================================================
    // # TO BIND IMAGE
    // ========================================================================
	@InitBinder
    public void initBinder(WebDataBinder binder)
    {
        binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
    }
}