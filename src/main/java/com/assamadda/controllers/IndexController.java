package com.assamadda.controllers;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.assamadda.exception.BadRequestException;
import com.assamadda.models.MasterOrganisation;
import com.assamadda.models.MasterQualification;
import com.assamadda.models.TransSubscribe;
import com.assamadda.payloads.JsonResponse;
import com.assamadda.payloads.PageableObjectPayload;
import com.assamadda.services.CaptchaService;
import com.assamadda.services.IndexService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * IndexController
 */
@RestController
@RequestMapping("/")
public class IndexController {

    @Autowired
    IndexService indexService;

    @Autowired
	private CaptchaService captchaService;

    // ========================================================================
    // # PUBLIC INDEX PAGE
    // ========================================================================
    @GetMapping(value = "")
    public ModelAndView getIndex(ModelAndView mv, @RequestParam("pageNo") Optional<Integer> pageNo) {
        mv = new ModelAndView("unlogged/index");
        int page = pageNo.orElse(0);

        PageableObjectPayload jobData = indexService.getPageableJobsForIndexPage(page);
        mv.addObject("jobData", jobData);

        return mv;
    }

    // ========================================================================
    // # PUBLIC JOB BY DISTRICT PAGE
    // ========================================================================
    @GetMapping(value = "district/{district}")
    public ModelAndView getJobsByDistrict(ModelAndView mv, @PathVariable("district") String district,@RequestParam("pageNo") Optional<Integer> pageNo) {
        mv = new ModelAndView("unlogged/district_job");
        int page = pageNo.orElse(0);

        PageableObjectPayload jobData = indexService.getPageableJobsByDistrict(district, page);
        mv.addObject("jobData", jobData);
        mv.addObject("district_slug", district);
        mv.addObject("job_type", district.replace("-", " "));

        return mv;
    }

    // ========================================================================
    // # PUBLIC JOB BY SECTOR PAGE
    // ========================================================================
    @GetMapping(value = "sector/{sector}")
    public ModelAndView getJobsBySector(ModelAndView mv, @PathVariable("sector") String sector,@RequestParam("pageNo") Optional<Integer> pageNo) {
        mv = new ModelAndView("unlogged/sector_job");
        int page = pageNo.orElse(0);

        PageableObjectPayload jobData = indexService.getPageableJobsBySector(sector, page);
        mv.addObject("jobData", jobData);
        mv.addObject("sector_slug", sector);
        mv.addObject("job_type", sector.replace("-", " "));

        return mv;
    }

    // ========================================================================
    // # PUBLIC JOB BY QUALIFICATION PAGE
    // ========================================================================
    @GetMapping(value = "qualification/{qualification}")
    public ModelAndView getJobsByQualification(ModelAndView mv, @PathVariable("qualification") String qualification,@RequestParam("pageNo") Optional<Integer> pageNo) {
        mv = new ModelAndView("unlogged/qualification_job");
        int page = pageNo.orElse(0);

        PageableObjectPayload jobData = indexService.getPageableJobsByQualification(qualification, page);
        mv.addObject("jobData", jobData);
        mv.addObject("qualification_slug", qualification);
        mv.addObject("job_type", qualification.replace("-", " "));

        return mv;
    }

    // ========================================================================
    // # PUBLIC JOB BY QUALIFICATION LIST
    // ========================================================================
    @GetMapping(value = "qualification")
    public ModelAndView getQualificationList(ModelAndView mv) {
        mv = new ModelAndView("unlogged/qualification_view_list");
        List<MasterQualification> qualificationList = indexService.getQualificationList();
        mv.addObject("qualificationList", qualificationList);
        return mv;
    }

    // ========================================================================
    // # PUBLIC JOB BY CATEGORY PAGE
    // ========================================================================
    @GetMapping(value = "category/{category}")
    public ModelAndView getJobsByCategory(ModelAndView mv, @PathVariable("category") String category,@RequestParam("pageNo") Optional<Integer> pageNo) {
        mv = new ModelAndView("unlogged/category_job");
        int page = pageNo.orElse(0);

        PageableObjectPayload jobData = indexService.getPageableJobsByCategory(category, page);
        mv.addObject("jobData", jobData);
        mv.addObject("category_slug", category);
        mv.addObject("job_type", category);

        return mv;
    }

    // ========================================================================
    // # PUBLIC JOB SEARCH PAGE
    // ========================================================================
    @GetMapping(value = "job-search")
    public ModelAndView getSearchedJobs(ModelAndView mv, @RequestParam("pageNo") Optional<Integer> pageNo, @RequestParam("searchText") String searchText) {
        mv = new ModelAndView("unlogged/job_search");
        int page = pageNo.orElse(0);

        PageableObjectPayload jobData = indexService.getSearchedJobs(searchText, page);
        mv.addObject("jobData", jobData);

        mv.addObject("searchText", searchText);

        return mv;
    }

    // ========================================================================
    // # PUBLIC JOB DETAILS
    // ========================================================================
    @GetMapping(value = "job/{slug}")
    public ModelAndView getJobDetailsBySlug(ModelAndView mv, @PathVariable("slug") String slug) {
        mv = new ModelAndView("unlogged/job_details");

        PageableObjectPayload jobData = indexService.getJobDetailsBySlug(slug);
        mv.addObject("jobData", jobData);

        return mv;
    }

    // ========================================================================
    // # PUBLIC ASSAm GK DETAILS
    // ========================================================================
    @GetMapping(value = "assam-general-knowledge/{slug}")
    public ModelAndView getArtivleDetailsBySlug(ModelAndView mv, @PathVariable("slug") String slug) {
        mv = new ModelAndView("unlogged/article_details");

        PageableObjectPayload articleData = indexService.getArticleDetailsBySlug(slug);
        mv.addObject("articleData", articleData);

        return mv;
    }

    // ========================================================================
    // # GET ORGANISATION IMAGE
    // ========================================================================
    @GetMapping(value = "/organisationImage/{organisationId}")
    public void getOrganisationImage(@PathVariable("organisationId") Long organisationId, HttpServletResponse response,
            HttpServletRequest request) throws ServletException, IOException {

        MasterOrganisation organisation = indexService.getOrganisation(organisationId);
        response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
        response.getOutputStream().write(organisation.getImage());
        response.getOutputStream().close();
    }

    // ========================================================================
    // # METHOD TO CREATE NEW SUBSCRIBER
    // ========================================================================
    @PostMapping(value = { "/create-subscriber" })
    public ResponseEntity<JsonResponse> createSubscriber(@Valid @ModelAttribute TransSubscribe transSubscribe,
            BindingResult bindingResult, HttpServletRequest request) throws BindException {

        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        String response = request.getParameter("g-recaptcha-response");
        String captchaVerify = captchaService.processResponse(response);

        if (!"success".equals(captchaVerify)) {
            bindingResult.rejectValue("email", "error.email", "* Captcha not verified.");
        }

        TransSubscribe existSubscriber = indexService.findSubscriberByEmail(transSubscribe.getEmail());

        if (!Objects.equals(existSubscriber, null)) {
            bindingResult.rejectValue("email", "error.email", "* Email already subscribed.");
        }

        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        } else {
            JsonResponse res = indexService.saveSubscriber(transSubscribe);

            if (Objects.equals(res.getResult(), true)) {
                return ResponseEntity.ok().body(res);
            } else {
                throw new BadRequestException("Operation Failed");
            }
        }
    }
}