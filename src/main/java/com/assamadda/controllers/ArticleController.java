package com.assamadda.controllers;

import java.util.Objects;

import javax.validation.Valid;

import com.assamadda.exception.BadRequestException;
import com.assamadda.models.TransArticle;
import com.assamadda.payloads.JsonResponse;
import com.assamadda.payloads.PageableObjectPayload;
import com.assamadda.services.ArticleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;

/**
 * ArticleController
 */
@RestController
@RequestMapping("/contributor")
public class ArticleController {

    @Autowired
    ArticleService articleService;

    // ========================================================================
    // # PAGE ARTICLES
    // ========================================================================
    @GetMapping(value = { "/articles" })
    public ModelAndView pageArticleArticles(ModelAndView mv) {
        mv = new ModelAndView("contributor/articles");
        return mv;
    }

    // ========================================================================
    // # PAGE CREATE ARTICLE
    // ========================================================================
    @GetMapping(value = { "/articles/create" })
    public ModelAndView createArticle(ModelAndView mv) {
        mv = new ModelAndView("contributor/create_update_article");

        PageableObjectPayload articleData = articleService.getAllObjectsForCreatingArticle();
        mv.addObject("articleData", articleData);

        mv.addObject("pagePurpose", "CREATE ARTICLE");
        return mv;
    }

    // ========================================================================
    // # METHOD TO CREATE/UPDATE NEW ARTICLE
    // ========================================================================
    @PostMapping(value = { "/create-article" })
    public ResponseEntity<JsonResponse> createArticle(@Valid @ModelAttribute TransArticle transArticle, BindingResult bindingResult)
            throws BindException {
        Long articleId = transArticle.getArticleId();

        if (!bindingResult.hasErrors()) {
            JsonResponse res = articleService.createArticle(transArticle);

            if (Objects.equals(res.getResult(), true)) {
                if (Objects.equals(articleId, null)) {
                    res.setMessage("Article Created Successfully.");
                } else {
                    res.setMessage("Article Updated Successfully.");
                }
                return ResponseEntity.ok().body(res);
            } else {
                throw new BadRequestException("Operation Failed");
            }
        } else {
            throw new BindException(bindingResult);
        }
    }
    

    // ========================================================================
    // # GET ARTICLE LIST BY DATE RANGE
    // ========================================================================
    @GetMapping(value = { "/get-contributor-article-list" })
    public ResponseEntity<JsonResponse> getArticleList() {

        JsonResponse res = articleService.getArticleList();

        if (Objects.equals(res.getResult(), true)) {
            return ResponseEntity.ok().body(res);
        } else {
            throw new BadRequestException("Operation Failed");
        }
    }

    // ========================================================================
    // # PAGE UPDATE ARTICLE
    // ========================================================================
    @GetMapping(value = { "/articles/update/{articleSlug}" })
    public ModelAndView updateArticle(@PathVariable("articleSlug") String articleSlug, ModelAndView mv) {
        mv = new ModelAndView("contributor/create_update_article");

        PageableObjectPayload articleData = articleService.getAllObjectsForCreatingArticle();
        mv.addObject("articleData", articleData);

        mv.addObject("articleSlug", articleSlug);
        mv.addObject("pagePurpose", "UPDATE ARTICLE");
        return mv;
    }

    // ========================================================================
    // # DELETE ARTICLE
    // ========================================================================
    @PutMapping(value = { "/delete-article" })
    public ResponseEntity<JsonResponse> deleteArticle(@RequestParam("articleId") Long articleId) {

        JsonResponse res = articleService.deleteArticle(articleId);

        if (Objects.equals(res.getResult(), true)) {
            res.setMessage("Article Deleted Successfully.");
            return ResponseEntity.ok().body(res);
        } else {
            throw new BadRequestException("Operation Failed");
        }
    }

    // ========================================================================
    // # GET ARTICLE DETAILS By SLUG
    // ========================================================================
    @GetMapping(value = { "/get-contributor-article-data" })
    public ResponseEntity<JsonResponse> getArticleArticleDataBySlug(@RequestParam(value = "articleSlug") String articleSlug) {

        JsonResponse res = articleService.getArticleArticleDataBySlug(articleSlug);

        if (Objects.equals(res.getResult(), true)) {
            return ResponseEntity.ok().body(res);
        } else {
            throw new BadRequestException("Operation Failed");
        }
    }

    // ========================================================================
    // # TO BIND IMAGE
    // ========================================================================
	@InitBinder
    public void initBinder(WebDataBinder binder)
    {
        binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
    }
}